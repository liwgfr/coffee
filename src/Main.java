import java.io.IOException;
import java.util.Scanner;

// TODO Refactor Code & Realise More User-Friendly Controller / Add Ingredients' amount.

public class Main {
    public static void main(String[] args) throws InterruptedException, IOException {
        Coffee currentCoffee = new Coffee();
        boolean flag = true;
        Scanner in = new Scanner(System.in);
        razmetka("Welcome to the Basic Command-line Coffee shop!");
        System.out.println();
        System.out.println("Please, Choose one of the following drinks: ");
        for (int i = 0; i < CoffeeList.values().length; i++) {
            System.out.print(CoffeeList.values()[i] + " ");
            if (i % 3 == 0) {
                System.out.println();
            }
        }
        razmetka("Please, press 'E' to exit the Coffee Machine!");
        System.out.println();
        String s = in.next().toUpperCase();
        if (s.length() == 1 && s.charAt(0) == 'E') {
            System.out.println("Thank you for using us!");
            System.exit(0);
        }
        if (CoffeeList.hasIngredient(s)) {
            currentCoffee.setCurrentCoffee(s);
            CoffeeList coffee;
            coffee = CoffeeList.valueOf(s);
            System.out.println("Well Done!" + '\n' + s.toUpperCase() + " requires such Ingredients: ");
            System.out.println(coffee.showIngredients().toString().replaceAll("[\\[\\]]", ""));
        } else {
            System.out.println("Hmm... I guess your input is not correct! ");
            Thread.sleep(1000);
            Runtime.getRuntime().exec("clear");
            flag = false;
            main(new String[0]);

        }
        razmetka("Please, press 'F' to start cooking or to start again :P");
        System.out.println();
        razmetka("Please, press 'E' to exit the Coffee Machine!");
        System.out.println();
        s = in.next().toUpperCase();
        if (s.length() == 1 && s.charAt(0) == 'F') {
            Coffee.progressBar(currentCoffee, flag);
            System.out.println("Here is your " + currentCoffee.getCurrentCoffee());
            System.out.println("Input 'R' to Restart or 'E' to Exit!");
            String z = in.next();
            if (z.length() == 1 && z.charAt(0) == 'R') main(new String[0]);
            if (z.length() == 1 && z.charAt(0) == 'E') {
                System.out.println("Thank you for using us!");
                System.exit(0);
            }
            if (z.length() != 1 && (z.charAt(0) != 'R' || z.charAt(0) != 'E')) {
                System.out.println("Hmm... I guess your input is not correct! ");
                Thread.sleep(1000);
                Runtime.getRuntime().exec("clear");
                main(new String[0]);
            }
        }
        if (s.length() == 1 && s.charAt(0) == 'E') {
            System.out.println("Thank you for using us!");
            System.exit(0);
        }
        if (s.length() != 1 && (s.charAt(0) != 'E' || s.charAt(0) != 'F')) {
            System.out.println("Hmm... I guess your input is not correct! ");
            Thread.sleep(1000);
            Runtime.getRuntime().exec("clear");
            main(new String[0]);
        }
    }
    private static void razmetka(String out) {
        for (int i = 0; i < 25; i++) {
            System.out.print('~');
        }
        System.out.print(" " + out + " ");
        for (int i = 0; i < 25; i++) {
            System.out.print('~');
        }
    }
}
