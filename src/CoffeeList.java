import java.util.HashSet;

public enum CoffeeList {
    CAPUCCINO(Coffee.callIngredients("CAPUCCINO")),
    DOUBLE_ESPRESSO(Coffee.callIngredients("DOUBLE_ESPRESSO")),
    ESPRESSO(Coffee.callIngredients("ESPRESSO")),
    FLAT_WHITE(Coffee.callIngredients("FLAT_WHITE")),
    MMS_COFFEE(Coffee.callIngredients("MMS_COFFEE")),
    OREO_COFFEE(Coffee.callIngredients("OREO_COFFEE")),
    RAF(Coffee.callIngredients("RAF"));

    HashSet<String> set;

    CoffeeList(HashSet<String> ar) {
        set = ar;
    }
    public HashSet<String> showIngredients() {
        return set;
    }
    public static boolean hasIngredient(String s) {
        for (CoffeeList c : CoffeeList.values()) {
            if (c.toString().equals(s)) {
                return true;
            }
        }
        return false;
    }
}

