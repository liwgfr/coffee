import me.tongfei.progressbar.ProgressBar;

import java.io.IOException;
import java.util.*;

public class Coffee {
    public static HashSet<String> callIngredients(String k) {
        Map<String, HashSet<String>> map = new HashMap<>();
        String[] list = {"Milk", "Sugar", "Ice Cream", "Chocolate", "Strawberry", "Blackberry", "Mint", "Hot Chocolate", "Oreo Cookies", "MM's", "Lavender"};
        Random r = new Random();
        int x = 3;
        int b = 9;
        int rand = x + (int) (Math.random() * b);
        HashSet<String> a = new HashSet<>();
        a.add("Water");
        a.add("Coffee Beans");
        for (int i = 0; i < rand; i++) {
            a.add(list[r.nextInt(list.length)]);
        }
        map.put(k, a);
        return map.get(k);
    }
    private String currentCoffee;

    public static void progressBar(Coffee coffee, boolean flag) throws IOException, InterruptedException {
        if (!flag) {
            Main.main(new String[0]);
        }
        int a = 200;
        int b = 1000;
        String[] str_array = {"Adding water...", "Mixing some stuff", coffee.getCurrentCoffee() + " on do"};
        List<String> list = new ArrayList<>(Arrays.asList(str_array));

        int n = (int) (a + Math.random() * b);
        int cnt = 0;
        String out;
        try (ProgressBar pb = new ProgressBar("Coffee Status", 100)) {
            for (int i = 0; i < 100; i++) {
                int z = (int) (1 + Math.random() * 3);
                if (pb.getCurrent() >= 100) {
                    break;
                }
                pb.stepBy(z);
                Thread.sleep(n);
                Random r = new Random();
                if (pb.getCurrent() >= 0 && pb.getCurrent() <= 10) {
                    pb.setExtraMessage("Starting... trying...");
                }
                if (pb.getCurrent() >= 10 && pb.getCurrent() <= 22) {
                    if (cnt == 0) {
                        int v = r.nextInt(list.size());
                        out = list.get(v);
                        pb.setExtraMessage(out);
                        list.remove(v);
                        cnt++;
                    }
                }
                if (pb.getCurrent() > 22 && pb.getCurrent() <= 57) {
                    if (cnt == 1) {
                        int v = r.nextInt(list.size());
                        out = list.get(v);
                        pb.setExtraMessage(out);
                        list.remove(v);
                        cnt++;
                    }
                }
                if (pb.getCurrent() > 57 && pb.getCurrent() <= 90) {

                    if (cnt == 3) {
                        int v = r.nextInt(list.size());
                        out = list.get(v);
                        pb.setExtraMessage(out);
                        list.remove(v);
                        cnt++;
                    }
                }
                if (pb.getCurrent() > 90 && pb.getCurrent() <= 100) {
                    pb.setExtraMessage("Ending up");
                }
            }
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public Coffee(){
    }
    public String getCurrentCoffee() {
        return currentCoffee;
    }
    public void setCurrentCoffee(String currentCoffee) {
        this.currentCoffee = currentCoffee;
    }
}
